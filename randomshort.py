# EJERCICIO 9.2. MINIPRÁCTICA 1

import webapp
import random

formulario   = """
    <form name="formulario" action="" method="POST">
    <p>Introduce la url a acortar: <input type="text" name="contenido"></p>
    <input type="submit" value="Enviar"></form>
"""


class Randomshort(webapp.webApp):
    urls = {}

    def parse(self, received):
        recibido = received.decode()
        method = recibido.split(' ')[0]  # método
        resource = recibido.split(' ')[1]  # recurso
        if method == "POST":
            body = recibido.split('\r\n\r\n')[1]  # cuerpo
        else:
            body = None

        return method, resource, body

    def process(self, analyzed):
        method, resource, body = analyzed

        if method == "GET":
            if resource == "/":
                http = "200 OK"
                html = ("<html><body>" + "Lista de URLS ya acortadas: " + str(self.urls) + "\n""<br>" + formulario + "</body></html>")
            else:
                if resource in self.urls:
                    http = "200 OK"
                    html = "<html><body> Recurso: " + resource + "..." \
                           + '<meta http-equiv="refresh" content="2;url=' + (self.urls[resource]) + \
                           '">' "</body></html></body></html>"
                else:
                    http = "404 Not Found"
                    html = "No encontrado"
        elif method == 'POST':
            url = body.split('contenido=')[1]
            print(url)
            rand = random.randint(1,1000)
            short = "/"+str(rand)
            sorti = "http://localhost:1234" + short
            print(short)
            print(sorti)

            if url == "":
                http = "404 Not Found"
                html = "No encontrado"
            else:
                if not url.startswith('https://') or not url.startswith('http://'):
                    url = "https://" + url

                self.urls[short] = url
                http = "200 OK"
                html = f"<html><body>Url original: <a href='{url}'>{url}</a>" \
                           f"<br><br>Url ya acortada: <a href='{url}'>http://localhost:1234{short}</a></body></html>"

        else:
            http = "404 Not Found"
            html = "No encontrado"

        return http, html


if __name__ == "__main__":
    testWebApp = Randomshort("localhost", 1234)
